#This is the python script to implement the task E of Assignment2 'Comparison & Benchmark'
#Import the python packages required for this exercise

import numpy as np
import matplotlib.pyplot as plt #for graphics
import fireworks.nbodylib.dynamics as fdyn #Library with the three acceleration function we need to implement
from fireworks.particles import Particles #To manage particles position and masses
import fireworks.ic as fic #Library to set initial conditions
import time #to measure the computational time of the processes

N = np.array([2,10,100,300,500,1000])#Number of particles in the simulation
mmin = 2 #minimum mass
mmax = 17 #maximum mass
vmin = -15 #minimum velocity
vmax = 15 #maximum velocity
xmin = 0 #minimum position
xmax = 100 #maximum position

#define a function which takes as imput different number of particles and returns the delta time for the computation of each acceleration
def acceleration_comparison(N):
    particles = fic.ic_random_uniform(N,mmin=mmin,mmax=mmax,vmin=vmin,vmax=vmax,xmin=xmin,xmax=xmax)#generate particles

    #estimate acceleration using the direct method

    t1_direct = time.perf_counter()
    acc_direct = fdyn.acceleration_direct(particles, 0.)
    t2_direct = time.perf_counter()
    dt_direct = t2_direct - t1_direct # time elapsed from t1 to t2
    #print(dt_direct)

    #estimate acceleration using the direct vectorized method

    t1_vectorized = time.perf_counter()
    acc_vectorized = fdyn.acceleration_direct_vectorised(particles, 0.)
    t2_vectorized = time.perf_counter()
    dt_vectorized = t2_vectorized - t1_vectorized # time elapsed from t1 to t2
    #print(dt_vectorized)

    #Estimate acceleration using fast-multipole tree-based gravity solver

    t1_tree = time.perf_counter()
    acc_tree = fdyn.acceleration_pyfalcon(particles, 0.)
    t2_tree = time.perf_counter()
    dt_tree = t2_tree - t1_tree # time elapsed from t1 to t2
    #print(dt_tree)

    return (dt_direct,dt_vectorized,dt_tree)

#create the list to store data
acceleration_direct = []
acceleration_vectorized = []
acceleration_tree = []

for i in N:#iteration to create the array for the plot
    accdirect, accvect, acctree = acceleration_comparison(i)
    #acc_i = acceleration_comparison(i)[1]
    acceleration_direct.append(accdirect)
    #acceleration_direct.append(*acc_i)
    acceleration_vectorized.append(accvect)
    acceleration_tree.append(acctree)

acceleration_direct = np.array(acceleration_direct)
acceleration_vectorized = np.array(acceleration_vectorized)
acceleration_tree = np.array(acceleration_tree)

plt.plot(N,acceleration_direct,label = 'Direct method')
plt.plot(N,acceleration_vectorized, '--',label = 'Vectorized method')
plt.plot(N,acceleration_tree, label = 'Tree method')

plt.xlabel('Number of particles')
plt.ylabel('Computational time [s]')
plt.legend()
plt.tight_layout()
plt.savefig('assignment2_taskE.pdf')
#plt.show()

