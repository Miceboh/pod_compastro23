"""
====================================================================================================================
Collection of functions to estimate the Gravitational forces and accelerations (:mod:`fireworks.nbodylib.dynamics`)
====================================================================================================================

This module contains a collection of functions to estimate acceleration due to
gravitational  forces.

Each method implemented in this module should follow the input-output structure show for the
template function  :func:`~acceleration_estimate_template`:

Every function needs to have two input parameters:

    - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
    - softening, it is the gravitational softening. The parameters need to be included even
        if the function is not using it. Use a default value of 0.

The function needs to return a tuple containing three elements:

    - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
        this element is mandatory it cannot be 0.
    - jerk, time derivative of the acceleration, it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx3 numpy array.
    - pot, gravitational potential at the position of each particle. it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx1 numpy array.


"""
from typing import Optional, Tuple
import numpy as np
import numpy.typing as npt
from fireworks.particles import Particles
import math as math

try:
    import pyfalcon
    pyfalcon_load=True
except:
    pyfalcon_load=False

def acceleration_estimate_template(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This an empty functions that can be used as a basic template for
    implementing the other functions to estimate the gravitational acceleration.
    Every function of this kind needs to have two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """

    acc  = np.zeros(len(particles))
    jerk = None
    pot = None

    return (acc,jerk,pot)


def acceleration_direct(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This an empty functions that can be used as a basic template for
    implementing the other functions to estimate the gravitational acceleration.
    Every function of this kind needs to have two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """

    def acc_twobody(i,j):#def a function which calculates the acceleration between two bodies
        ac  = np.zeros([3])
        dx = particles.pos[i,0] - particles.pos[j,0]#calculate x,y and z distance between the two bodies
        dy = particles.pos[i,1] - particles.pos[j,1]
        dz = particles.pos[i,2] - particles.pos[j,2]

        r = np.sqrt( dx ** 2 + dy ** 2 + dz ** 2)

        ac[0] = ( particles.mass[j] * dx ) / ( r ** 3 )
        ac[1] = ( particles.mass[j] * dy ) / ( r ** 3 )
        ac[2] = ( particles.mass[j] * dz ) / ( r ** 3 )

        return ac

    N = len(particles.mass)#define the number of particles
    acc  = np.zeros([N, 3])
    jerk = None
    pot = None

    for i in range(N):#iterate over all particles i and j
        for j in range(N):
            if i != j:#remove the case in which i consider twice the same particle
                acc_ij = acc_twobody(i,j)#calculate the acceleration between each couple
                acc[i,0] -= acc_ij[0]#append the value to compute the total acceleration
                acc[i,1] -= acc_ij[1]
                acc[i,2] -= acc_ij[2]

    return (acc,jerk,pot)


def acceleration_direct_vectorised(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This an empty functions that can be used as a basic template for
    implementing the other functions to estimate the gravitational acceleration.
    Every function of this kind needs to have two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """

    N = len(particles.mass)
    acc  = np.zeros([N,3])
    jerk = None
    pot = None
    dx = np.zeros([N, N])
    dy = np.zeros([N, N])
    dz = np.zeros([N, N])
    r = np.zeros([N, N])

    #Cartesian component of the i,j particles distance
    dx = (particles.pos[:,0].reshape(1,N) - particles.pos[:,0].reshape(N,1))
    dy = (particles.pos[:,1].reshape(1,N) - particles.pos[:,1].reshape(N,1))
    dz = (particles.pos[:,2].reshape(1,N) - particles.pos[:,2].reshape(N,1))

    #Distance module
    r = np.sqrt(np.multiply(dx,dx) + np.multiply(dy,dy) + np.multiply(dz,dz))

    #Cartesian component of the i,j force
    acc[:,0] -= (particles.mass.reshape(1,N) @ (np.divide(dx,np.multiply(r,np.multiply(r,r)), out=np.zeros_like(dx), where=r!=0))).reshape(N)
    acc[:,1] -= (particles.mass.reshape(1,N) @ (np.divide(dy,np.multiply(r,np.multiply(r,r)), out=np.zeros_like(dy), where=r!=0))).reshape(N)
    acc[:,2] -= (particles.mass.reshape(1,N) @ (np.divide(dz,np.multiply(r,np.multiply(r,r)), out=np.zeros_like(dz), where=r!=0))).reshape(N)

    return (acc,jerk,pot)


def acceleration_pyfalcon(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration following the fast-multipole gravity Dehnen2002 solver (https://arxiv.org/pdf/astro-ph/0202512.pdf)
    as implementd in pyfalcon (https://github.com/GalacticDynamics-Oxford/pyfalcon)

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: None, the jerk is not estimated
        - Pot: a Nx1 numpy array containing the gravitational potential at each particle position
    """

    if not pyfalcon_load: return ImportError("Pyfalcon is not available")

    acc, pot = pyfalcon.gravity(particles.pos,particles.mass,softening)
    jerk = None

    return acc, jerk, pot

def acceleration_jerk_direct(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:

    def acc_twobody(i,j):#def a function which calculates the acceleration between two bodies
        ac  = np.zeros([3])
        dx = particles.pos[i,0] - particles.pos[j,0]#calculate x,y and z destance between the two bodies
        dy = particles.pos[i,1] - particles.pos[j,1]
        dz = particles.pos[i,2] - particles.pos[j,2]

        r = np.sqrt( dx ** 2 + dy ** 2 + dz ** 2)

        ac[0] = ( particles.mass[j] * dx ) / ( r ** 3 )
        ac[1] = ( particles.mass[j] * dy ) / ( r ** 3 )
        ac[2] = ( particles.mass[j] * dz ) / ( r ** 3 )

        return ac
    
    def jerk_twobody(i,j):#def a function which calculates the acceleration between two bodies
        jk  = np.zeros([3])
        dx = particles.pos[i,0] - particles.pos[j,0]#calculate x,y and z destance between the two bodies
        dy = particles.pos[i,1] - particles.pos[j,1]
        dz = particles.pos[i,2] - particles.pos[j,2]
        dvx = particles.vel[i,0] - particles.vel[j,0]#calculate x,y and z destance between the two bodies
        dvy = particles.vel[i,1] - particles.vel[j,1]
        dvz = particles.vel[i,2] - particles.vel[j,2]

        r = np.sqrt( dx ** 2 + dy ** 2 + dz ** 2)
        xv = dx*dvx + dy*dvy + dz*dvz

        jk[0] = particles.mass[j] * (dvx/r**3 - 3*(xv*dx/r**5))
        jk[1] = particles.mass[j] * (dvy/r**3 - 3*(xv*dy/r**5))
        jk[2] = particles.mass[j] * (dvz/r**3 - 3*(xv*dz/r**5))

        return jk

    N = len(particles.mass)#define the number of particles
    acc  = np.zeros([N, 3])
    jerk = np.zeros([N, 3])
    pot = None

    for i in range(N):#itarate ove all particles i and j
        for j in range(N):
            if i != j:#remove the case in which i consider twice the same particle
                acc_ij = acc_twobody(i,j)#calculate the acceleration between each couple
                acc[i,0] -= acc_ij[0]#append the value to compute the total acceleration
                acc[i,1] -= acc_ij[1]
                acc[i,2] -= acc_ij[2]
                jerk_ij = jerk_twobody(i,j)#calculate the jerk between each couple
                jerk[i,0] -= jerk_ij[0]#append the value to compute the total jerk
                jerk[i,1] -= jerk_ij[1]
                jerk[i,2] -= jerk_ij[2]

    return (acc,jerk,pot)